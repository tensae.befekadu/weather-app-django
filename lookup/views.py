from django.shortcuts import render
import json
import requests


def home(request):

    api_request = requests.get(
        "http://api.openweathermap.org/data/2.5/weather?q=Addis%20Ababa,ET&APPID=2dbae74016bb620c25e768b38b15f2ed&units=Metric")

    try:
        api = json.loads(api_request.content)
    except Exception as e:
        api = "Error ....."

    return render(request, 'home.html', {'api': api})


def about(request):
    return render(request, 'about.html', {})
